[[!meta robots="noindex"]]

Guidelines:

- "your Persistence" when referring to a volume that has already been
  created on the USB stick, for example when unlocking it.
- "Persistence" (without article) the rest of the time, either when
  referring to the feature or the volume itself.
- "the Persistence feature" when we need to refer to the feature
  explicitly.
- "the Persistent folder" because it's its name in the file system.
- "GnuPG Persistence feature" for each of the options that can be
  enabled in Persistence.
- "persistent" when referring to the property of something saved in
  persistent.

Examples:

- Enter your passphrase to unlock your Persistence. (from Greeter)

- Your Persistence is unlocked. Restart Tails to lock it again. (from Greeter)

- Cannot delete the Persistence on %s while in use. (from Persistence setup)

- Upgrade your Tails USB stick and keep your Persistence. (from [[install/download]])

- This method might in rare occasions break the file system of your Persistence. (from [[doc/first_steps/shutdown]])

- Create an apt-sources.list.d folder in your Persistence. (from [[doc/first_steps/additional_software]])

- Freeing space in your Persistence. (from [[doc/first_steps/additional_software]])

- Follow these steps to create a new password database and save it in your Persistence for use in future working sessions. (from [[doc/encryption_and_privacy/manage_passwords]])

- When starting Tails, enable your Persistence. (from [[doc/encryption_and_privacy/manage_passwords]])

- If you choose [Install Every Time], the package is saved in your Persistence and will be reinstalled automatically every time you start Tails. (from [[doc/first_steps/additional_software]])

- Enable & use Persistence (from [[doc/first_steps/persistence]])

- Create Persistence (from [[install/win/usb]])

- You can create Persistence in the free space left on the USB stick. (from [[doc/first_steps/persistence]])

- Restart on your backup Tails and create Persistence on it. (from [[doc/first_steps/persistence/copy]])

- To create Persistence or change its configuration, choose Applications → Tails → Persistence. (from [[doc/first_steps/persistence/configure]])

- An attacker in possession of your USB stick can know that there is Persistence on it. (from [[doc/first_steps/persistence/warnings]])

- If Persistence is detected on the USB stick, an additional section appears in Tails Greeter. (from [[doc/first_steps/startup_options]])

- Warnings about Persistence (from [[doc/first_steps/persistence]])

- To understand better how Persistence works, see our design document. (from [[support/faq]])

- The simplest way to carry around the documents that you want to use with Tails encrypted is to use the Persistence feature. (from [[doc/encryption_and_privacy/encrypted_volumes]])

- To store your GnuPG keys and configuration across separate working sessions, you can activate the GnuPG Persistence feature. (from [[doc/encryption_and_privacy/gpgapplet/decrypt_verify]])

- Save the database as keepassx.kdbx in the Persistent folder. (from [[doc/encryption_and_privacy/manage_passwords]])

- Read the documentation on Persistence to learn which settings can be made persistent across separate working sessions. (from [[doc/first_steps/introduction_to_gnome_and_the_tails_desktop]])
