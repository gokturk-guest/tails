# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: tails-l10n@boum.org\n"
"POT-Creation-Date: 2019-08-29 15:15+0200\n"
"PO-Revision-Date: 2018-02-07 18:01+0000\n"
"Last-Translator: emmapeel <emma.peel@riseup.net>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: ru\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=n%10==1 && n%100!=11 ? 0 : n%10>=2 && n"
"%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Weblate 2.10.1\n"

#. type: Plain text
#, no-wrap
msgid "[[!meta title=\"Delete the persistent volume\"]]\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "<div class=\"caution\">\n"
msgstr "<div class=\"caution\">\n"

#. type: Plain text
#, no-wrap
msgid ""
"<p>This technique is fast but might not prevent a strong attacker from\n"
"recovering files from the old persistent volume using advanced [[data\n"
"recovery techniques|encryption_and_privacy/secure_deletion#why]].</p>\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"<p>To securely delete the persistent volume, you have to [[securely\n"
"delete the entire USB stick|encryption_and_privacy/secure_deletion#erase-device]],\n"
"which is a much slower operation.</p>\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "</div>\n"
msgstr "</div>\n"

#. type: Bullet: '  1. '
msgid ""
"Start Tails from the USB stick on which you want to delete the persistent "
"volume."
msgstr ""

#. type: Plain text
#, no-wrap
msgid "     Do not enable the persistent volume in <span class=\"application\">Tails Greeter</span>.\n"
msgstr ""

#. type: Bullet: '  1. '
msgid ""
"Choose <span class=\"menuchoice\"> <span class=\"guimenu\">Applications</"
"span>&nbsp;▸ <span class=\"guisubmenu\">Tails</span>&nbsp;▸ <span class="
"\"guimenuitem\">Delete persistent volume</span></span>."
msgstr ""

#. type: Bullet: '  1. '
msgid "Click <span class=\"guilabel\">Delete</span>."
msgstr ""
